//
//  ViewController.h
//
//  Created by Javier Alonso Gutierrez on 14/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JAAcordeonViewController.h"

@interface ViewController : UIViewController <JAAcordeonViewControllerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *numero;
- (IBAction)add:(id)sender;
- (IBAction)remove:(id)sender;

- (IBAction)cambiar:(id)sender;
@end
